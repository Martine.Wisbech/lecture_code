package lecture7;

public class Question {

	String question;
	String solution;
	
	Question(String question, String solution){
		this.question = question;
		this.solution = solution;
	}
	
	String getQuestion() {
		return question;
	}
	
	String getSolution() {
		return solution;
	}

	public String getQuestionWithSolution() {
		return getQuestion()+" "+getSolution();
	}
}
