package lecture7;

public class Addition implements Operator {

	@Override
	public char getOperator() {
		return '+';
	}

	@Override
	public int doOperaion(int num1, int num2) {
		return num1+num2;
	}

}
