package lecture7;

/**
 * This interface represents a simple Operator that takes as input 2 integer number values
 * and produces an integer output value
 * 
 * Notable examples are: +(plus), -(minus), *(multiply) and %(modulo)
 * Division can also be used as long as we round the answer to nearest integer.
 * Bitwise operators also exist but are harder to do in desimal numbers.
 * 
 * @author Martin Vatshelle
 *
 */
public interface Operator {

	/**
	 * This method returns the one character symbol that is used to represent the operator
	 * @return
	 */
	char getOperator();
	
	/**
	 * This method performs the operation defined by this class on two input numbers
	 * e.g. for + this computes num1 + num2 = ...
	 * @param num1 - First input number
	 * @param num2 - Second input number
	 * @return - the result from performing this operation.
	 */
	int doOperaion(int num1, int num2);
}
