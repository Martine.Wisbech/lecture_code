package lecture6;

public class UseLibrary {

	public static void main(String[] args) {
		//make two books
		Book inf102Book = new Book("Robert",376,"Algorithms");
		Book myThesis = new Book("Martin", 210, "New width parameters");
		//make empty library
		Library martinsShelf = new Library();
		//add the two books to the library
		martinsShelf.add(inf102Book);
		martinsShelf.add(myThesis);
		
		//checking if a book is in the library using the original reference
		//that was used when the library were made works
		System.out.println("Using same reference: "+martinsShelf.hasBook(inf102Book));

		//make a new book with the right information
		Book myWish = new Book("Martin", 210, "New width parameters");

		//this returns false if we do not have the right .equals method
		System.out.println("Using equal object :"+martinsShelf.hasBook(myWish));
		

	}

}
