package lecture6;

import java.util.ArrayList;
import java.util.Objects;

public class Library {

	ArrayList<Book> books;
	
	public Library() {
		books = new ArrayList<>();
	}
	
	public void add(Book book) {
		books.add(book);
	}
	
	public boolean hasBook(Book book) {
		return books.contains(book);
	}

	@Override
	public int hashCode() {
		return Objects.hash(books);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Library other = (Library) obj;
		return Objects.equals(books, other.books);
	}
}
