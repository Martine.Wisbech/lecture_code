package grid;

import java.util.Optional;

public interface IGrid<T> extends Iterable<Optional<T>> {

	void set(int row, int col, T elem);
	
	T get(int row, int col);
	T get(Location loc);

	void fill(T elem);

	Iterable<Location> locations();
}
