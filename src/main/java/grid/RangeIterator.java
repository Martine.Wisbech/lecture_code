package grid;

import java.util.Iterator;

public class RangeIterator implements Iterator<Integer> {

	Range range;
	int next;

	public RangeIterator(Range range) {
		this.range = range;
		next = range.start;
	}
	
	@Override
	public boolean hasNext() {
		return next<=range.stop;
	}

	@Override
	public Integer next() {
		int current = next;
		next += range.step;
		return current;
	}

}
