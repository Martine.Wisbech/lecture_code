package grid;

public class UseRange {

	public static void main(String[] args) {
		for(int i : range(6)) {
			System.out.println(i);
		}

	}

	private static Iterable<Integer> range(int i) {
		return new Range(i);
	}

}
