package grid;

import java.util.Iterator;

public class Range implements Iterable<Integer> {

	int start;
	int stop;
	int step;
	
	Range(int n){
		start = 0;
		stop = 5;
		step = 1;
	}
	
	@Override
	public Iterator<Integer> iterator() {
		return new RangeIterator(this);
	}

}
