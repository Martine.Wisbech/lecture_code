package grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

public class Grid<T> implements IGrid<T>{

	private int rows;
	private int cols;
	private ArrayList<Optional<T>> grid;
	
	Grid(int rows, int cols){
		if(rows<=0) {
			throw new IllegalArgumentException("Grids need at least 1 row, you supplied "+rows);
		}
		this.rows = rows;
		if(cols<=0) {
			throw new IllegalArgumentException("Grids need at least 1 column, you supplied "+cols);
		}
		this.cols = cols;
		grid = new ArrayList<Optional<T>>();
		for(int row=0; row<rows; row++) {
			for(int col=0; col<cols; col++) {
				grid.add(Optional.empty());
			}
		}
	}
	
	@Override
	public T get(int row, int col) {
		Optional<T> value = grid.get(indexOf(row, col));
		return value.get();
	}

	@Override
	public T get(Location loc) {
		return get(loc.row, loc.col);
	}
	
	@Override
	public void set(int row, int col, T elem) {
		Optional<T> value = Optional.of(elem);
		grid.set(indexOf(row,col),value);
	}
	
	private int indexOf(int row, int col) {
		if(row<0 || row >= rows)
			throw new IndexOutOfBoundsException("Illegal row value");
		if(col<0 || col >= cols)
			throw new IndexOutOfBoundsException("Illegal column value");
		return col+row*cols;
	}

	@Override
	public void fill(T elem) {
		for(Location loc : locations()) {
			set(loc, elem);
		}
	}

	public void set(Location loc, T elem) {
		set(loc.row,loc.col,elem);
	}

	@Override
	public Iterator<Optional<T>> iterator() {
		return grid.iterator();
	}

	@Override
	public Iterable<Location> locations() {
		return new GridIterable(rows,cols);
	}

}
