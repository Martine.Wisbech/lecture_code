package grid;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;

class GridTest {

	@Test
	void test() {
		IGrid<Integer> grid = new Grid<Integer>(9,9);
		grid.set(3,2,7);
		assertEquals(7,grid.get(3, 2));
		grid.set(3,5,9);
		assertEquals(9,grid.get(3, 5));

		assertEquals(7,grid.get(3, 2));
}

	@Test
	void test2() {
		IGrid<Integer> grid = new Grid<Integer>(9,9);
		grid.set(3,2,7);
		grid.set(2,3,9);
		assertEquals(7,grid.get(3, 2));
		assertEquals(9,grid.get(2, 3));
	}
	
	@Test 
	void testNeighbours(){
		Location loc = new Location(3,2);
		assertEquals(3,loc.row);
		assertEquals(2,loc.col);
		Location neighbour = GridDirection.getNeighbour(loc, GridDirection.NORTH);
		assertEquals(loc.col, neighbour.col);
		assertEquals(loc.row-1, neighbour.row);

		neighbour = GridDirection.getNeighbour(loc, GridDirection.EAST);
		assertEquals(loc.row, neighbour.row);
		assertEquals(loc.col+1, neighbour.col);

	}
	
	@Test
	void testNegativeValueInConstructor() {
		try {
			Grid<String> grid = new Grid<String>(3,-1);
		} catch (IllegalArgumentException e) {
			return;
		}
		fail();
	}
	
	@Test
	void testIndexOutOfBounds() {
		IGrid<String> grid = new Grid<String>(4,3);
		grid.fill("Hei");
		assertEquals("Hei",grid.get(3, 2));
		
		assertThrows(IndexOutOfBoundsException.class, () -> grid.get(5, 3), 
				"should not be able to access indexes outside of the grid");
		assertThrows(IndexOutOfBoundsException.class, () -> grid.get(2, 3), 
				"should not be able to access indexes outside of the grid");
	}
	
	@Test
	void testIterate() {
		IGrid<String> grid = new Grid<String>(4,3);
		grid.fill("Hei");
		for(Optional<String> s : grid) {
			assertEquals("Hei", s.get());
		}
		
		int count = 0;
		for(Location loc : grid.locations()) {
			assertEquals("Hei",grid.get(loc));
			count++;
		}
		assertEquals(4*3,count);
	}
}
